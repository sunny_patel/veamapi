/**
 * Lib
 */

var exports = module.exports = {};
var uuid = require('node-uuid'),
    doc = require('dynamodb-doc'),
    dynamo = new doc.DynamoDB();

exports.createRestaurant = function(event, context) {
  var tableName = 'veamRestaurants';
  if (event.hasOwnProperty('payload')) {
    if (event.payload.hasOwnProperty('Item')) {
      event.payload.TableName = tableName;
      event.payload.Item.restaurantId = uuid.v4();
      console.log(event);
      dynamo.putItem(event.payload, context.done());
    }

  } else {
    return context.error("Missing proper payload");
  }
}
