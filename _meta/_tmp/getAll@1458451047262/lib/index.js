/**
 * Lib
 */

var exports = module.exports = {};
var _tableNames = {
  'restaurants': 'veamRestaurants'
};
var aws = require('aws-sdk'),
    docClient = new aws.DynamoDB.DocumentClient(),
    uuid = require('node-uuid'),
    doc = require('dynamodb-doc'),
    dynamo = new doc.DynamoDB();

exports.createRestaurant = function(event, cb) {
  var tableName = _tableNames.restaurants;
  if (event.hasOwnProperty('payload')) {
    if (event.payload.hasOwnProperty('Item')) {
      event.payload.TableName = _tableNames['restaurants'];
      event.payload.Item.restaurantId = uuid.v4();
      return dynamo.putItem(event.payload, cb);
    }

  } else {
    return context.error("Missing proper payload");
  }
}

exports.findById = function(event, cb) {
  if (event.hasOwnProperty('key')) {
    event.TableName = _tableNames['restaurants'];
    event.Key = event.key;
    return docClient.get(event, cb);
  }
}
/**
 * Comparison Operators: 'EQ | NE | IN | LE | LT | GE | GT | BETWEEN | NOT_NULL | NULL | CONTAINS | NOT_CONTAINS | BEGINS_WITH'
 */
exports.findByName = function(event, cb) {
  if (event.hasOwnProperty('name')) {
    var params = {}
    params.TableName = _tableNames['restaurants'];
    params.IndexName = "name-index";
    params.KeyConditionExpression = '#name = :nameVal';
    params.ExpressionAttributeNames = {
      "#name": "name"
    };
    params.ExpressionAttributeValues = {
      ":nameVal": event.name
    };
    return docClient.query(params, cb);
  }
}

exports.getAllRestaurants = function(event, cb) {
  params.TableName = _tableNames['restaurants'];
  return docClient.scan(params, cb);
}
