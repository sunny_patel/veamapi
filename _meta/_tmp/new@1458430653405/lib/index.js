/**
 * Lib
 */

var exports = module.exports = {};
var uuid = require('node-uuid'),
    doc = require('dynamodb-doc'),
    dynamo = new doc.DynamoDB();

exports.createRestaurant = function(event, context) {
  var tableName = 'veamRestaurants';
  if (event.hasOwnProperty('payload')) {
    if (event.payload.hasOwnProperty('Item')) {
      event.payload.TableName = 'veamRestaurants';
      //event.payload.Item.restaurantId = uuid.v4();
      return dynamo.putItem(event.payload, context.done());
    }

  } else {
    return context.error("Missing proper payload");
  }
}

exports.test = function(event, context) {
  console.log("sunny");
  console.log(event);
}
