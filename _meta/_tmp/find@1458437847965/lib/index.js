/**
 * Lib
 */

var exports = module.exports = {};
var _tableNames = {
  'restaurants': 'veamRestaurants'
};
var uuid = require('node-uuid'),
    doc = require('dynamodb-doc'),
    dynamo = new doc.DynamoDB();

exports.createRestaurant = function(event, cb) {
  var tableName = _tableNames.restaurants;
  if (event.hasOwnProperty('payload')) {
    if (event.payload.hasOwnProperty('Item')) {
      event.payload.TableName = _tableNames['restaurants'];
      event.payload.Item.restaurantId = uuid.v4();
      return dynamo.putItem(event.payload, cb);
    }

  } else {
    return context.error("Missing proper payload");
  }
}

exports.findById = function(event, cb) {
  if (event.hasOwnProperty('key')) {
    event.TableName = _tableNames['restaurants'];
    event.Key = event.key;
    return dynamo.getItem(event, cb);
  }
}
/**
 * Comparison Operators: 'EQ | NE | IN | LE | LT | GE | GT | BETWEEN | NOT_NULL | NULL | CONTAINS | NOT_CONTAINS | BEGINS_WITH'
 */
exports.find = function(event, cb) {
  if (event.hasOwnProperty('conditions')) {
    event.TableName = _tableNames['restaurants'];
    event.KeyConditions = [];
    event.conditions.forEach(function(val){
      Object.keys(val).forEach(function(key){
        event.KeyConditions.push(dynamo.Condition(
          key,
          "IN",
          val[key]
        ));
      });
    });
    return dynamo.query(event, cb);
  }
}
