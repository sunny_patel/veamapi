/**
 * Lib
 */

var exports = module.exports = {};
var _tableNames = {
  'restaurants': 'veamRestaurants'
};
var uuid = require('node-uuid'),
    doc = require('dynamodb-doc'),
    dynamo = new doc.DynamoDB();

exports.createRestaurant = function(event, cb) {
  var tableName = _tableNames.restaurants;
  if (event.hasOwnProperty('payload')) {
    if (event.payload.hasOwnProperty('Item')) {
      event.payload.TableName = _tableNames.restaurants;
      event.payload.Item.restaurantId = uuid.v4();
      return dynamo.putItem(event.payload, cb);
    }

  } else {
    return context.error("Missing proper payload");
  }
}

exports.find = function(event, cb) {
  if (event.hasOwnProperty('Key')) {
    event.payload.TableName = _tableNames.restaurants;
    console.log(event);
    return dynamo.getItem(event, cb);
  }
}
